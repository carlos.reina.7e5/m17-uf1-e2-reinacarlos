﻿using System;
using System.Collections.Generic;

namespace M17_UF1_E2_ReinaCarlos
{
    class Game : GameEngine
    {
        MatrixRepresentation mr = new MatrixRepresentation(10,10);

        protected override void Start()
        {
            mr.CleanTheMatrix();
        }

        protected override void Update()
        {
            var list = new List<int[]>();
            for (var i = mr.TheMatrix.GetLength(0) - 1; i >= 0; i--)
            {
                for (var j = mr.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    if (mr.TheMatrix[i, j] != '0' && mr.TheMatrix[i, j] != ' ')
                    {
                        list.Add(new int[] { i, j });
                    }
                }
            }

            foreach (var i in list)
            {
                if (i[0] + 1 < mr.TheMatrix.GetLength(0))
                {
                    (mr.TheMatrix[i[0], i[1]], mr.TheMatrix[i[0] + 1, i[1]]) = ('0', mr.TheMatrix[i[0], i[1]]);
                }

                else mr.TheMatrix[i[0], i[1]] = '0';
            }

            InsertLetterInMatrix();

            mr.printMatrix();
        }
                    
        public void InsertLetterInMatrix()
        {
            var rnd = new Random();
            var x = false;

            do
            {
                var ranNum = rnd.Next(0, mr.TheMatrix.GetLength(0));
                if (mr.TheMatrix[0, ranNum] == '0')
                {
                    mr.TheMatrix[0, ranNum] = RandomLetter(rnd);
                    x = true;
                }

            } while (!x);

        }

        public char RandomLetter(Random rnd)
        {
            char c;
            do
            {
                c = (char)rnd.Next(65, 90);
            } while (c == '0');

            return c;
        }

        protected override void Exit()
        {
            base.Exit();
        }
    }
}
