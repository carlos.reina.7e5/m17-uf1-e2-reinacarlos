﻿using System;

namespace M17_UF1_E2_ReinaCarlos
{
    public class MatrixRepresentation
    {

        private char[,] _theMatrix;
        public char[,] TheMatrix
        {
            set
            {
                // _theMatrix = value;
                _theMatrix = clippingMatrix(value);

            }
            get
            {
                return _theMatrix;
            }
        }


        private int sizeX, sizeY;

        public MatrixRepresentation()
        {
            initMatrix(null, null);
        }
        public MatrixRepresentation(int width, int height)
        {
            initMatrix(width, height);
        }

        public void initMatrix(int? _sizeX, int? _sizeY)
        {
            sizeX = (_sizeX.HasValue) ? _sizeX.Value : 9;
            sizeY = (_sizeY.HasValue) ? _sizeY.Value : 9;
            _theMatrix = new char[sizeX, sizeY];
        }


        public void CleanMatrix()
        {
            initMatrix(sizeX, sizeY);
        }

        public void printMatrix()
        {
            Console.Clear();
            for (int x = 0; x < _theMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < _theMatrix.GetLength(1); y++)
                {
                    Console.Write($" {_theMatrix[x, y]}");
                }
                Console.WriteLine();
            }
        }

        public void CleanTheMatrix()
        {
            _theMatrix = blankMatrix();
        }

        public char[,] blankMatrix()
        {
            char[,] tm = new char[_theMatrix.GetLength(0), _theMatrix.GetLength(1)];

            for (int x = 0; x < tm.GetLength(0); x++)
            {
                for (int y = 0; y < tm.GetLength(1); y++)
                {
                    tm[x, y] = '0';
                }
            }
            return tm;
        }

        public char[,] clippingMatrix(char[,] toClipper)
        {
            int limitX = (toClipper.GetLength(0) > _theMatrix.GetLength(0)) ? _theMatrix.GetLength(0) : toClipper.GetLength(0);
            int limitY = (toClipper.GetLength(1) > _theMatrix.GetLength(1)) ? _theMatrix.GetLength(1) : toClipper.GetLength(1);

            for (int x = 0; x < limitX; x++)
            {
                for (int y = 0; y < limitY; y++)
                {
                    _theMatrix[x, y] = toClipper[x, y];
                }
            }
            return _theMatrix;
        }
    }
}
